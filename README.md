# Fatality News

Since GitLab currently doesn't support group-level wiki, this repo is intended as a quick hack for fatality-news group wide engineering wikis and documentations.

## Project Summary

Fatality news is the most accurate signal / noise-free in under testing situations and when the government covers details of death cases. It also spreads fast because bad news tends to travel faster than good news.

**We would like to present fatality news to our users along with its context so users can take informed-decisions.**

See [ideation here](https://kawalcovid19.slack.com/archives/C010GP27CDN/p1585299171015100)

## Long Term Vision

The vision would be to provide **insight based on context from news**. Example of another use case: news about hospital challenges such as limited APD (alat perlindungan diri) so that we know which hospital need help.

## Important Links
* [Product SSOT](https://quip.com/yYikAVfbUCsB/Product-SSOT-Kawal-COVID-19#QJTACAgXqJS)
* [Workflow](https://kawalcovid19.quip.com/QibnAelNbpiH/Fatality-News-Content-Workflow)
* [News Tagging Detail](https://kawalcovid19.quip.com/QibnAelNbpiH/Fatality-News-Content-Workflow#bcBACACTyNK)
* [Fact Checking Process](https://kawalcovid19.quip.com/QibnAelNbpiH/Fatality-News-Content-Workflow#bcBACA1aZOu)
* [Mockup / Design](https://kawalcovid19.quip.com/LXjxACI0mysW/Fatality-News-UX-Design-Doc)
* [List of News Site as Source](https://kawalcovid19.quip.com/5q2oAYj47mO6/List-of-News-Site-as-Source)

# Architecture
```mermaid
graph TD;
  A[News Site]-->B[News Scrapper];
  B-->C[(News DB)];
  B-->D[(Blog Storage)];
  D-->E[Tagging Tool<br>BasicAI];
  E-->F[Tag Exporter];
  F-->C;
  C-->G[Analyzer and Validator];
  G-->C;
  C-->H[Table and Visualization <br>on KawalCovid19.id Website];
```
Description of each component:
* **News Scrapper**: component which regularly (every 6 hour) scrap news from news sites.
* **News DB**: store 1) news raw data, 2) tagging data, 3) news + tag data ready to be presented.
* **Blob Storage**: Where we store scrapping result in form of txt files for tagging.
* **Tagging Tool - BasicAI**: a web-based tool for volunteer to tag context in news.
* **Tag Exporter**: a batch script to export tagging result into database.
* **Analyzer and Validator**: a batch script to validate duplicate cases, also to find agreement if users tag the same news but contradict.
* **Table and Visualization**: where we present the information to the users, along with original link to the news.

Note:
* Scrapping directly write to database so that data cleansing / ETL happen on the scrapper right away.
* Raw news data, tagging data, and presentation news data will be on the same database, but different table
* We use plain txt file on blob storage as recommended text format to be loaded to Basic AI for tagging by volunteer. Subject to change if BasicAI has simpler method.